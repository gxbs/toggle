import Adw from 'gi://Adw';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';

import { ToggleWindow } from './window.js';

/**
 * This class is the foundation of most complex applications.
 * It handles many things crucial for app developers:
 *  - Registers a D-Bus name for your application
 *  - Makes sure the application process is unique
 *  - Registers application resources like icons, ui files, menus, and shortcut dialogs.
 *  - Allows app developers to easily set up global actions and shortcuts
 *
 * Here we're using AdwApplication, which provides extra functionality by automatically
 * loading custom styles and initializing the libadwaita library.
 *
 * For more information on AdwApplication and its parent classes, see:
 *  - https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Application.html
 *  - https://docs.gtk.org/gtk4/class.Application.html
 *  - https://docs.gtk.org/gio/class.Application.html
 */
export class ToggleApplication extends Adw.Application {
    private window?: ToggleWindow;

    /**
     * When subclassing a GObject, we need to register the class with the
     * GObject type system. We do this here in the static initializer block,
     * as it needs to be run before everything else.
     *
     * For more information on subclassing and the abilities of
     * `GObject.registerClass()`, see https://gjs.guide/guides/gobject/subclassing.html
     */
    static {
        GObject.registerClass(this);
    }

    constructor() {
        super({
            application_id: pkg.name,
            flags: Gio.ApplicationFlags.DEFAULT_FLAGS,
        });

        /**
         * GActions are the most powerful tool a developer can use
         * to react to user input. There are different types of actions,
         * and actions can be attached to UI and shortcuts in multiple ways.
         *
         * In this example we're using GSimpleAction, as it's simplest
         * implementation of actions provided by gio.
         *
         * For more information, see:
         *  - https://gnome.pages.gitlab.gnome.org/gtk/gio/iface.Action.html
         *  - https://gnome.pages.gitlab.gnome.org/gtk/gio/iface.ActionGroup.html
         *  - https://gnome.pages.gitlab.gnome.org/gtk/gio/iface.ActionMap.html
         *
         * The application class implements GActionGroup and GActionMap,
         * providing us the ability to add actions directly to the application.
         * When we want to refer to the action elsewhere, we use the name of the
         * action group we used as a prefix. Actions directly added to applications
         * are prefixed with `app`.
         */
        const quit_action = new Gio.SimpleAction({ name: 'quit' });
        quit_action.connect('activate', () => {
            this.quit();
        });

        this.add_action(quit_action);
        this.set_accels_for_action('app.quit', ['<Control>q']);

        const show_about_action = new Gio.SimpleAction({ name: 'about' });
        show_about_action.connect('activate', () => {
            new Adw.AboutWindow({
                transient_for: this.active_window,
                modal: true,
                application_name: _('Toggle'),
                application_icon: pkg.name,
                developer_name: 'Dallas Strouse',
                version: pkg.version,
                license_type: Gtk.License.GPL_3_0,

                debug_info: 'TODO: write a get_debug_info function',
                debug_info_filename: 'toggle-debuginfo.txt',
                issue_url: 'https://gitlab.com/orowith2os/toggle/-/issues/new',
                support_url: 'https://matrix.to/#/#toggle-application:fedora.im',

                // Translators: Replace "translator-credits" with your names, one name per line
                translator_credits: _('translator-credits'),
                developers: ['Dallas Strouse <dastrouses@gmail.com>'],
                designers: ['Bart Gravendeel', 'Brage Fuglseth'],
                artists: ['TODO: make an icon'],
                copyright: '© 2023 Dallas Strouse',
            }).present();
        });

        this.add_action(show_about_action);

        Gio._promisify(Gtk.UriLauncher.prototype, 'launch', 'launch_finish');
    }

    // When overriding virtual functions, the function name must be `vfunc_$funcname`.
    public vfunc_activate(): void {
        if (!this.window) {
            this.window = new ToggleWindow({ application: this });
        }

        this.window.present();
    }
}
