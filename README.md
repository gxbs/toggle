# Toggle

For an overview of the features we plan to keep from Tweaks and move to Settings, see FEATURES.md.

## TODO

- [x] Convert the mockups to an AdwApplicationWindow
- [ ] Implement the logic for reading the tweaks
  - [ ] Button layout
  - [ ] Fonts
  - [ ] Headerbar actions
- [ ] Split the mockups into several files for each menu, and implement the TS bits
- [ ] Make an icon
- [ ] Add a debug info function
- [ ] Add Blueprint support to `data/ui`

## Preview

Appearance | Window
---|---
![A mockup of the appearance tab, showing a list of appearance preferences](mockups/toggle-appearance-preview.png) | ![A mockup of the window tab, showing a list of window preferences](mockups/toggle-window-preview.png)

Devices | Startup
---|---
![A mockup of the devices tab, showing a list of device preferences](mockups/toggle-devices-preview.png) | ![A mockup of the startup tab, showing a grid of startup application cards](mockups/toggle-startup-preview.png)

## Motivation

GNOME Tweaks is barely maintained, many of the settings are broken in newer GNOME versions, and many of the settings have already been moved to GNOME Settings. Since many users still rely on a couple of settings that Tweaks provides, we need a solution.

## Goal

Our goal is to completely replace GNOME Tweaks with a modernized experience. This includes:

- Cleaned up settings, nothing that doesn't work / is already implemented in GNOME Settings
- A new UI, made with GTK4 and Libadwaita
- Shipped as a Flatpak, hosted on Flathub

Non-Flatpak builds of Toggle will **not** be supported.
